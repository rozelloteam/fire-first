/**
 * @flow
 */

import {AppRegistry} from 'react-native';
import Root from './src/root'

AppRegistry.registerComponent('fireFirst', () => Root);
