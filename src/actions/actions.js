import * as types form '../constants/ActionTypes';

function addTodoAction(title, hour) {
  return {type: types.ADD_TODO, title, hour}
}
