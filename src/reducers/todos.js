import * as types from '../constants/ActionTypes';
import Utils from '../utils';


function getEndTime(hour) {
  var dt = new Date();
  dt.setHours(hour?hour:(dt.getHours()+1));
  dt.setMinutes(0);
  return dt.getTime();
}

const initialState = {
  isFetchingAllTodos: false,
  data: [
    {
      id: Utils.GUID(),
      title: 'Make a todo!',
      endTime: getEndTime(),
      completedTime: 0,
      completed: false
    },
    {
      id: Utils.GUID(),
      title: 'Complete a todo!',
      endTime: getEndTime(),
      completedTime: 0,
      completed: false
    }
  ]
}

export default function todos(state=[], action) {

  switch (action.type) {
    case types.ADD_TODO:
      return Object.assign({}, state, {
        data: [
          {
            id: Utils.GUID(),
            title: action.title,
            endTime: getEndTime(action.hour),
            completed: false
          },
          ...state.data
        ]
      });

    default:
        return state;
  }
}
